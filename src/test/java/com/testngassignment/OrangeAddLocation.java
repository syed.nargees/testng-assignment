package com.testngassignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class OrangeAddLocation {
	WebDriver driver;
	 @BeforeClass
	    void setUp() {
	        driver = new ChromeDriver();
	        driver.manage().window().maximize();
	    }

	    /* Quit the driver after execution */
	    @AfterClass
	    void tearDown() {
	        driver.quit();
	    }
	    @Test(priority=1)
	    public void launchApplication() {
	    	driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
	    String title=driver.getTitle();
	    System.out.println(title);
	    

		}
	    @Test(priority=2)
	    public void loginIntoApplocation() {
	    	driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	    	driver.findElement(By.name("username")).sendKeys("Admin");

			driver.findElement(By.name("password")).sendKeys("admin123");
			driver.findElement(By.xpath("//button[text()=' Login ']")).click();

		}
	    @Test(priority=4)
	    public void addlocation() throws InterruptedException {
	    	driver.findElement(By.xpath("//*[text()='Admin']")).click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//span[text()='Organization ']")).click();
			driver.findElement(By.partialLinkText("Locations")).click();

			driver.findElement(By.xpath("//button[text()=' Add ']")).click();
			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("QA Selenium Wave 6");
			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]")).sendKeys("Renoldsbug");
			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]")).sendKeys("ohio");
			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]")).sendKeys("43068");
			driver.findElement(By.xpath("//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow']")).click();
			driver.findElement(By.xpath("//*[contains(text(),'United States')]")).click();

			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]")).sendKeys("999999999");
			driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]")).sendKeys("425001");
			driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[1]"))
					.sendKeys("2793 Taylor Rd Ext, Reynoldsburg, OH 43068, United States");
			driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("hello");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[@type='submit']")).click();
			String text=driver.findElement(By.xpath("//div[text()='QA Selenium Wave 6']")).getText();
			System.out.println(text);
			

		}
	    
	    @Test (priority=4)
	    public void deleteDetails() throws Exception {
	    	
   	driver.findElements(By.xpath("//input[@class='oxd-input oxd-input--active']")).get(2).sendKeys("Renoldsbug");
	driver.findElement(By.xpath("//button[@type='submit']")).click();
	Thread.sleep(3000);
	driver.findElements(By.xpath("//button[@class='oxd-icon-button oxd-table-cell-action-space']")).get(0).click();
	driver.findElement(By.xpath("//i[@class='oxd-icon bi-trash oxd-button-icon']")).click(); 
	    }
	    
	    @Test(priority=5)
	    public void logoutmetho()
	    {driver.findElement(By.xpath("//i[@class=\"oxd-icon bi-caret-down-fill oxd-userdropdown-icon\"]")).click();
    	driver.findElement(By.xpath("//a[text()='Logout']")).click();
	    	//driver.findElement(By.xpath("//a[text()='Logout']")).click();
	    	driver.close();
	    }
	  
}

