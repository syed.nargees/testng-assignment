package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest2 {
	 @Test(dependsOnMethods = "newCustomer")
	  public void modifyCustomer() {
		  System.out.println("the customer will get modified");
	  }
	  @Test(dependsOnMethods="modifyCustomer")
	  public void createCustomer() {
		  System.out.println("the customer will get created");
	  }
	  @Test()
	  public void newCustomer() {
		  System.out.println("New customer will get created");
	  }
	  @BeforeMethod
	  public void beforeCustomer()
	  {
		  System.out.println("verify the customer");
	  }
	  @AfterMethod
	  public void afterMethod()
	  {
		  System.out.println("all transaction is done");
	  }
	  @BeforeClass
	  public void beforeClass()
	  {
		  System.out.println("start data base connrction ,launch browser");
	  }
	  @AfterClass
	  public void afterClass()
	  {
		  System.out.println("close data base connection and close connection");
	  }
}
