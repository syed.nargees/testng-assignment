package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GroupingOfTestcase {
	//smoketest
	  @Test(groups={"smoke test"})
	  public void createCustomer() {
		  System.out.println("the customer will get created");
	  }
	  //regresstion
	  @Test(groups={"regresstion test"})
	  public void newCustomer() {
		  System.out.println("New customer will get created");
	  }
	  //sanity
	  @Test(groups= {"usability test"})
	  public void modifyCustomer() {
		  System.out.println("the customer will get modified");
	  }
	  //smoketest
	  @Test(groups={"smoke test"})
	  public void changeCustomer()
	  {
		  System.out.println("the customer details will get changed");
	  }
	  
	  @BeforeClass
	  public void beforeClass()
	  {
		  System.out.println("start data base connrction ,launch browser");
	  }
	  @AfterClass
	  public void afterClass()
	  {
		  System.out.println("close data base connection and close connection");
	  }
	  
}
